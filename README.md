# Granny's recipes

Welcome to the Granny's website source code.
It shows you the best recipes in the world, for sure!
Here, you will find all information you need about how the website is built.

## Table of contents
  - [Overview](#overview)
  - [Prerequisites](#prerequiites)
  - [How does it work?](#how-does-it-work?)
  - [Documentation](#documentation)


## Overview
*TODO*

## Prerequisites
- Please make sure you get git installed on your PC;
- Store your git credentials for once
  ```sh
  les-recettes-de-mamie/ $ git config --global credential.helper store
  ```

## How does it work?
*TODO*

## Documentation
*TODO*
