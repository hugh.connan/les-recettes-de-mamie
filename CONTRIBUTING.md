# CONTRIBUTING

When contributing to this repository, please first discuss the change
you wish to make via a GitLab issue.

## If you are developer on this project

For any development, you must create a new branch.

Please make sure your changes are tracked by a GitLab Issue in the board issues
board, whether it has been selected for the current sprint or not;
Start all of your commit messages with one of the following keywords:
  - `Add`;
  - `Change`;
  - `Deprecate`;
  - `Remove`;
  - `Fix`;
  - `Security`.


On the same row, please write a meaningful commit message;

Two line breaks after, please prepend the reference of your GitLab issue;
Please, make sure your branch is rebased on the latest stable development branch
before any merge request.

**A commit example message**
```sh
Add a README.md


path/to/frontend/directory#1
```
